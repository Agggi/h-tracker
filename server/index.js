const cors = require('cors');
const mysql = require('mysql');
const express = require('express');
const app = express();

const db = mysql.createPool({
    host     : 'localhost',
    port : '6603',
    user     : 'root',
    password : 'admin',
    database : 'Habittracker'
});

//may need app.use(express.json());
app.use(cors());

app.get("/getHabit", (req, res) => {
    const sqlGetHabits = "select * from habit where user_id = ?";
    db.query(sqlGetHabits, [req.query.uid], (err, result) => {
        if (err) throw err;
        res.setHeader('Content-Type', 'application/json');
        res.end(JSON.stringify(result));
    });
})

app.listen(3001, () => {
    console.log('Server is running at port '+ 3001);
});

