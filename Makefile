install:
	cd ./server && npm install
	cd ./client && npm install

server:
	cd ./server && sudo docker-compose up -d && npm start

client:
	cd ./client && npm start

stop-docker:
	cd ./server && sudo docker-compose down && npm 


