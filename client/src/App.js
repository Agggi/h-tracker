
import { BrowserRouter as Router, Route, Switch, Link } from "react-router-dom";
import Habits from './components/Habits';
import Stats from './components/Stats';
import Account from './components/Account';




function App() {
    

    return (
        <Router>
            <div className="sidebar">
                <Link to="/" className="link">Habits</Link>
                <Link to="/stats" className="link">Stats</Link>
                <Link to="/account" className="link">Account</Link>
            </div>
            <Switch>
                <Route exact path="/">
                    <Habits />
                </Route>
                <Route exact path="/stats">
                    <Stats />
                </Route>
                <Route exact path="/account">
                    <Account />
                </Route>
            </Switch>
        </Router>
    );
}



export default App;
