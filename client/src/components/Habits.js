import Card from './Card';
import Popup from './Popup';
import { useState, useEffect } from 'react';
import axios from 'axios';
import { AiOutlinePlusSquare } from "react-icons/ai";


const Habits = () => {

    const [cards, setCards] = useState("Habits could not be loaded");   //for use effect to load habits from db
    const [visPop, setVisPop] = useState(false);    //for popup visibility

    /* Called once: After return to fetch data and replace first rendered html with fetched data as state */
    useEffect(() => {
        // credentials for testing purposes
        const user = 'Agu';
        const uid = '1';
        const serverAddr = 'http://localhost:3001';
        let rows = [];

        const promise = axios.get(serverAddr+'/getHabit?uid='+uid);
        promise.then(response => {      //response takes return value of former promise
            const habits = response.data;
            for (let i = 0; i < habits.length; i++) {
                const habit = habits[i];
                rows.push(<Card key={habit.id} id={habit.id} title={habit.title} />);
            }
            setCards(rows);
        })
        .catch(error => {
            console.log(error);
        });
    }, []);


    return (  
        <div className="main">
            <h1 id="title">HABITS</h1>
            <div id='cards'>{cards}</div>
            <AiOutlinePlusSquare id="addIcon" onClick={() => setVisPop(true)}/>
            <Popup visible={visPop} setVisible={setVisPop} content ="create new habit" userIn = "Drink water"/>
        </div>
    );
}
 
export default Habits;