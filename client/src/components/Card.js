import { GrCheckbox, GrCheckboxSelected } from 'react-icons/gr';


function Card(props) {
    const id = "habit_" + props.id;
    return(
        <div id={id} className="card">
            <p><GrCheckbox className="grcheckbox"/>{props.title}</p>
        </div>
    );
}

export default Card;