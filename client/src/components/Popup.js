import React from 'react'
import { AiOutlineClose } from "react-icons/ai";

function Popup(props) {
    let rtn ="";

    if (props.visible)
        rtn = <div className="popUp">
                <AiOutlineClose onClick={() => props.setVisible(false)}/>
                <p>{props.content}</p>
                <p>{props.userIn}</p>
            </div>
    return rtn;
}

export default Popup
